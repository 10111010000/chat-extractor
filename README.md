# chat-extractor

A livestream chat extractor that aggregates multiple inputs

## main.go

This file is the backend server

* pass messages between extractor and frontend html page
* serves frontend html page @ http://127.0.0.1:8839/


#### Building server

[Golang Downloads](https://go.dev/dl/)

[Git for windows Download](https://git-scm.com/download/win)

download and install golang and git(on git install change editor to nano, everything else default)

in powershell, navigate to directory that contains main.go

```shell
go mod init v0
go mod tidy
go build -0 comment-server.exe .
```

#### Running server

in powershell

```shell
comment-server.exe
```
press enter
leave window open

#### Adding chat element to obs

With the server running, drag this link -> [http://127.0.0.1:8839/](http://127.0.0.1:8839/) into obs preview window 

The comment element is in the bottom right corner of preview, nothing will show up until a comment is received.

## extractor.js manifest.json

A chrome plugin, is the extractor that sends messages to backend server

#### adding plugin to chrome

in chrome

* click extensions
* click manage extensions
* turn on developer mode (upper right corner of page)
* click load unpacked
* point it to the directory that contains manifest.json
* comment-extractor will be added to list, click details and give it site permissions


####  using chrome plugin

with the server running
* visit url with chat element
* select service
* click "start extractor"
