package main

import (
	_ "embed"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

var (
	//go:embed root.html
	rootHTML    []byte
	upgrader    = websocket.Upgrader{ReadBufferSize: 1024, WriteBufferSize: 1024}
	messageChan chan []byte
)

func start() {
	// http://localhost:8839/ : serve frontend
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if _, err := w.Write(rootHTML); err != nil {
			log.Fatalf("%v\n", err)
		}
	})
	// http://localhost:8839/wsc : send messages to frontend
	http.HandleFunc("/wsc", func(w http.ResponseWriter, r *http.Request) {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Printf("%v\n", err)
		}
		for {
			mcm := <-messageChan
			if err = conn.WriteMessage(1, mcm); err != nil {
				return
			}
		}
	})
	// http://localhost:8839/ws : listen for incoming messages
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Printf("%v\n", err)
		}
		for {
			msgType, msg, err := conn.ReadMessage()
			if err != nil {
				return
			}
			messageChan <- msg
			if err = conn.WriteMessage(msgType, []byte("received")); err != nil {
				return
			}
		}
	})
	fmt.Println("starting server")
	if err := http.ListenAndServe(":8839", nil); err != nil {
		log.Printf("%v\n", err)
	}
}

func main() {
	messageChan = make(chan []byte)
	start()
}
